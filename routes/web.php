<?php //קובץ ראוט

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//יצירת ראוט חדש שהופסנו בהרצאה 6
//נוסיף לראוט את המידלוויר שיוודא שרק שמי שהתחבר יוכל להיכנס
//הראוט הזה מפנה לכל הפונקציות שבקונטרולר 
//אם נרצה לפנות לפונקצייה ספציפית נצטרך לרשום את שם הקאנדידייטקונטרולר ואז לשים שטורדל ואז לכתוב את שם הפונקציה
Route::resource('candidates', 'CandidatesController')->middleware('auth');

//http://localhost/recruit/public/candidates

//הרצאה 8
//הוספת ראוט חדש לדאליט
Route::get('candidates/delete/{id}','CandidatesController@destroy')->name('candidate.delete'); // נגדיר שם לראוט


//הרצאה 9
//הוספת ראוט חדש לפונקצייה החדשה שהוספנו
//מכינסים בכתובת הראוט גם את הארגומנטים של הפונקציה מהקונטרולר בה משתמשים
Route::get('candidates/changeuser/{cid}/{uid?}','CandidatesController@changeUser')->name('candidate.changeuser');//בעזרת השם שנגדיר גם אם נשנה את היוראל הראוט ימשיך לעבוד


//תרגיל בית 9
//נוסיף לראוט את המידלוויר שיוודא שרק שמי שהתחבר יוכל להיכנס
//מכינסים בכתובת הראוט גם את הארגומנטים של הפונקציה מהקונטרולר בה משתמשים
Route::get('candidates/changestatus/{cid}/{sid}','CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth');

//תרגיל בית 11
//Route::get('MyCandidates','CandidatesController@showMyCandidates')->name('MyCandidates')->middleware('auth');

//הרצאה 12
Route::get('mycandidates','CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');


/*
//יצירת ראוט חדש
Route::get('/hello', function(){
    return 'Hello Larevel';
});
// ביוראל בדפדפן נכתוב http://localhost/recruit/public/hello


//יצירת ראוט שבו נקבל באינפוט פרמטר
// ההודעה שנקבל אם לא נקליד אי די היא הודעת שגיאה מובנת 404
//ולא הודעה שאנחנו רוצים שתופיע אם המשתמש לא מקליד אי די
Route::get('/student/{id}', function($id = 'No student found'){ // לא באמת נקבל את ההודעה הזאת אם משתמש לא יקליד אי די רוני סתם רשם
    return 'we got student with id  '.$id; //שרשור מחרוזות
});
//http://localhost/recruit/public/student/1


//כדי לתת תשובה למקרה והמשתמש לא מקליד אי די
// יודפס למסך הודעה שצריך להקליד אי די אם המשתמש לא הקליד
Route::get('/car/{id?}', function($id = null){ // חייב להוסיף סימן שאלה וערך דיפולטיבי כמו נאל
    if(isset($id)){       //האם קיבלתי את המשתנה איי די
        return "we got car $id"; //שרשור
    } else{
        return 'We need the id to find your car';
    }
});


Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});
 */

//תרגיל בית 5
Route::get('/users/{email}/{name?}', function($email,$name = 'name missing'){ 
    $email=$email;
    $name = $name;
    return view('users', compact('email','name'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//test
Route::get('interviews', 'InterviewsController@index')->name('interviews.index')->middleware('auth');
Route::get('myinterviews', 'InterviewsController@myInterviews')->name('interviews.myinterviews')->middleware('auth');

Route::get('interviews/create', 'InterviewsController@create')->name('interviews.create')->middleware('auth');
Route::post('interviews', 'InterviewsController@store')->name('interviews.store')->middleware('auth');
Route::get('interviews/changeCandidate/{iid}/{cid}', 'InterviewsController@changeCandidate')->name('interviews.change-candidate')->middleware('auth');
Route::get('interviews/changeUser/{iid}/{uid}', 'InterviewsController@changeUser')->name('interviews.change-user')->middleware('auth');