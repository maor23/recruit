<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //הוספה שלנו לתרגיל בית 6


class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert(
            [
            [
                'interview_date' => Carbon::now(),
                'interview_summary' => 'Bla Bla',
                'created_at' => Carbon::now(), 
                'updated_at'=>Carbon::now()
            ],
            [
                'interview_date' => Carbon::now(),
                'interview_summary' => 'Was a good interview',
                'created_at' => Carbon::now(), 
                'updated_at'=>Carbon::now()
            ],
            ]);
    }
}
