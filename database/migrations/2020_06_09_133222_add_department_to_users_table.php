<?php //נוסיף עמודת דיפראטמנט לטבלת יוזרס

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartmentToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('department_id')->nullable()->after('email')->unsigned()->index; //נוסיף עמודה של ססטוס איידי לטבלת קאנדיידט כי חייב שיהיה עמודה מקשרת בין הטבלאות
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('department_id');
        });
    }
}
