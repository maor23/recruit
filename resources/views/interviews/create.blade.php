@extends('layouts.app') <!--קובץ ויו --><!--נקשר לליאוט -->

@section('title', 'Create Interview') <!--נקשר לטייטל בליאוט -->

@section('content') <!--נקשר לקונטנט לליאוט -->


                <!--הוספה של טופס -->
                    <h1 class="text-center font-weight-bold text-black"> Create interviews</h1>
                        <form method = "post" action ="{{action('InterviewsController@store')}}"class="needs-validation" novalidate> <!--נוסיף פונקציית סטור --> 
                            @csrf <!--לאבטחת מידע -->
                            <div>
                                <label for = "interview_date">Interview date</label>
                                <input type = "date" name = "interview_date">
                            </div>    
                            <div>
                                <label for = "interview_summary">Interview summary</label>
                                <input type = "text" name = "interview_summary">
                            </div>
                            <div>
                        Candidate
                                    <select name="candidate_id">
                                    <option value=""></option>
                                    @foreach($candidates as $candidate)<!--שתיפתח רשימה שתראה את כל היוזרים הקיימים --><!--לולאה שמחליפה כל יוזרס ביוזר -->
                                        <option value="{{$candidate->id}}">{{$candidate->name}}</option>
                                    @endforeach
                                    </select>
                            <div>
                            <div>
                        User
                        <select name="user_id">
                        <option value=""></option>

                        @foreach($users as $user)<!--שתיפתח רשימה שתראה את כל היוזרים הקיימים --><!--לולאה שמחליפה כל יוזרס ביוזר -->
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                        </select>
                <div>
                                <input type = "submit" name = "submit" value = "Crete Interview">
                            </div>

                        </form>
@endsection
