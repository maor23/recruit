@extends('layouts.app') <!--קובץ ויו --><!--נקשר לליאוט -->

@section('title', 'Interviews')

@section('content')




<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div> 
@if (!$interviews->count())
<h3>You have no interviews</h3>
@endif

            <!--הוספה שלנו שהמידע יוצג בצורת טבלה -->
                <h1 class="text-center font-weight-bold text-black"> List of interviews</h1>
                <table class="table table-striped">
                <table class="table table-hover">
                <thead class="thead-dark"> 
                    <tr> <!--הגדרה של העמודות בטבלה -->
                    <th>id</th><th>Date</th><th>Summary</th><th>Candidate</th><th>User</th><th>Created</th><th>Updated</th>
                    </tr>
                    
                    <!-- the table data -->
                    @foreach($interviews as $interview) <!--לולאה שמחליפה כל קאנדידייטס בדאנדידייט -->
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$interview->id}}</td><!--בזכות הקומפקט שהוספנו בקונטרולר זה מזהה את המשתמש-->
                            <td>{{$interview->interview_date}}</td>
                            <td>{{$interview->interview_summary}}</td>
                            
                            <td> <!-- נוסיף עמודה עם קוד מועתק מבווטסטראפ וקוד ששינינו כדי שיהי עמודה של כל היוזרים תחת רשימה נפתחת -->
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--החלק של הפונקציונליות של הלחיצה בכפתור- בחירה מהרשימה -->
                                        @if(isset($interview->candidate_id))<!--שואלים את השאלה האם הוגדר לקאנדידייט יוזר איידי -->
                                            {{$interview->candidate->name}}<!--אם כן הוגדר אז כאן הריליישנשיפ שהגדרנו בא לידי ביטוי,נרצה שיופיע השם של היוזר שהוגדר לכל קנדידייט מסתתר פה ג'וין -->
                                        @else<!--אם לא הוגדר לקאנדידייט יוזר אידיי -->
                                            Assign candidate<!--זה מה שיופיע כברירת מחדל -->
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($candidates as $candidate)<!--שתיפתח רשימה שתראה את כל היוזרים הקיימים --><!--לולאה שמחליפה כל יוזרס ביוזר -->
                                        <a class="dropdown-item" href="{{route('interviews.change-candidate',[$interview->id,$candidate->id])}}">{{$candidate->name}}</a><!-- הויו מעביר לראוט שמעביר לקונטרולר שמעביר למודל שמעביר לבסיס הנתונים ששם הם נשמרים, במערך שמים את הפרמטרים שהפונקצייה רוצה לקבל -->
                                    @endforeach
                                    </div>
                                </div>
                            </td>
                            <td> <!-- נוסיף עמודה עם קוד מועתק מבווטסטראפ וקוד ששינינו כדי שיהי עמודה של כל היוזרים תחת רשימה נפתחת -->
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--החלק של הפונקציונליות של הלחיצה בכפתור- בחירה מהרשימה -->
                                        @if(isset($interview->user_id))<!--שואלים את השאלה האם הוגדר לקאנדידייט יוזר איידי -->
                                            {{$interview->user->name}}<!--אם כן הוגדר אז כאן הריליישנשיפ שהגדרנו בא לידי ביטוי,נרצה שיופיע השם של היוזר שהוגדר לכל קנדידייט מסתתר פה ג'וין -->
                                        @elseif($interview->candidate_id && $interview->candidate->user_id)
                                          {{$interview->candidate->owner->name}} 
                                        @else<!--אם לא הוגדר לקאנדידייט יוזר אידיי -->
                                            Assign user<!--זה מה שיופיע כברירת מחדל -->
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($users as $user)
                                        <a class="dropdown-item" href="{{route('interviews.change-user',[$interview->id,$user->id])}}">{{$user->name}}</a>
                                    @endforeach
                                    </div>
                                </div>
                            </td>
                            <td>{{$interview->created_at}}</td>
                            <td>{{$interview->updated_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
@endsection
