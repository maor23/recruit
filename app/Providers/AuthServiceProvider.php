<?php //ניצור גייט

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();



        // הרצאה 12
        //יוזר יכול לשנות סטטוס רק למועמדים שלו, רק קאנדידייטס שיש להם יוזר איידי עם אותו יוזר איידי של היוזר שמחובר כרגע - רק למועמדים האלה היוזר יכול לשנות את הסטטוס
        Gate::define('change-status', function ($user,$candidate) { //מגדירים בהתחלה את שם הגייט, ואז את שם המשתנים שנרצה שיכנסו לפונקציה,את המשתנה יוזר לא באמת צריך להכניס כי לארבל עושה את זה לבד
            return $user->id === $candidate->user_id; //בודק שהיוזר איידי שווה לקאנדידייט יוזר איידי
        });

        //רק מי שהוא מנג'ר יכול לשנות אוונר - והגרדנו בטבלה יוזרול שרק יוזר עם אידיי שווה 1 הוא מנג'ר לכן בינתיים רק הוא יכול לשנות אוונר למועמד
        Gate::define('assign-user', function ($user) {//מגדירים בהתחלה את שם הגייט, ואז את שם המשתנים שנרצה שיכנסו לפונקציה
            return $user->isManager(); //בודק שהיוזר הוא מנג'ר
        }); 

        Gate::define('create-interview', function ($user) {//מגדירים בהתחלה את שם הגייט, ואז את שם המשתנים שנרצה שיכנסו לפונקציה
            return $user->isManager() || $user->isAdmin(); //בודק שהיוזר הוא מנג'ר
        }); 


    }
}
