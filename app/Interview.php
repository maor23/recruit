<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = [ 'interview_date', 'interview_summary', 'candidate_id', 'user_id'];

    public function candidate() 
    {
        return $this->belongsTo('App\Candidate', 'candidate_id');
    }
    public function user() 
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
