<?php //קובץ מודל שיצרנו

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB; //ספרייה שהוספנו על מנת לבנות שאילתה


class Candidate extends Model
{
    //נדרד כדי לבצע מאס אסיינמנט - הרצאה שמונה במחברת
    protected $fillable = ['name','email']; // הוספה שלנו שכך נוכל לעדכן או ליצור הכל מבלי לציין במופרש בקונטרולר אילו שדות ספציפיים אנחנו מעונינים לייצר או לעדכן



    public function owner(){ //נדגיר פונקצייה שתחבר בין 2 הטלבאות שבשתי המודלים
        return $this->belongsTo('App\User','user_id'); //יצרנו את החיבור בין המודל הזה שהוא קאינדידיט לבין המודל יוזר כיוון הסלאש הוא קריטי - החיבור הוא בילונגס טו שזה בעצם וואן טו מאני הקשר הוא כך שמועמד שייך למשתמש
    } //מגדירים ישירות ללארבל מהי העמודה שמקשרת בין שתי המודלים יוזר וקאנדידייט שזאת העמודה יוזר אידיי

    public function status(){ 
        return $this->belongsTo('App\Status','status_id'); //מגדירים ישירות ללארבל מהי העמודה שמקשרת בין שתי המודלים קאנדידייט וסטטוס שזאת כמובן העמודה ססטוס אידיי שנמצאת בשתי הטבלאות
    }

    /*    
//תרגיל בית 11
//נשתמש בקואררי בילאדר שזה כלי שלל לארבל שמאפשר לבנות שאילתות של אסקיואל מבלי קוד של אסקיואל
    public static function userCandidate($uidConnect){ //יואיידיקונקט זה המשתנה שהגדרנו בקונטרולר שמציין את האיידי של היוזר המאומת שהתחבר כרגע
        $userCandidate = DB::table('candidates')->where('user_id',$uidConnect)->pluck('id'); //שאילתה שאומרת לך לטבלת קאנדידייטס, שהעמודה יוזר איידי תהפוך לפי המשתנה שהוגדר - יואידייקונקט, אותו הגדרנו בקונטרולר והוא מציין את האידיי רק של היוזר שמחובר כרגע, והפלאק אומר שנחזיר עמודה ספציפית מטבלת קאנדידייטס שהיא האיידי של כל מועמד 
        return self::find($userCandidate)->all(); //יחזיר מערך של איידי של כל המועמדים ששיכים לאותו יוזר שמחובר כרגע
    }
*/
  public function interview()
  {
      return $this->hasMany('App\Interview');
  }


}






