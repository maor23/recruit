<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate; //כדי שיזהה את המודל שיצרנו
use App\User; //כדי שיזהה את המודל שיצרנו
use Illuminate\Support\Facades\Auth; //תרגיל בית 11

use Illuminate\Support\Facades\Gate; // הרצאה 12


class InterviewsController extends Controller
{
    public function index() 
    {
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.index', compact('interviews', 'candidates', 'users'));
    }

    public function myInterviews() 
    {
        $user_id = Auth::id();
        $user = User::findOrFail($user_id);
        $interviews = $user->interviews;
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.index', compact('interviews', 'candidates', 'users'));
    }

    public function create() 
    {
        Gate::authorize('create-interview'); //נרשום את שם הגייט, את שם היוזר שמחובר כרגע לא צריך לכתוב כי לארבל מכירה אותו אוטומטית כאשר משתמשים בפונקציית גייט
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.create', compact('candidates', 'users'));
    }

    public function store(Request $request) 
    {
        Gate::authorize('create-interview');
        $data = $request->all();
        $interview = new Interview();
        $interview->create($data);
        return redirect('interviews');

    }

    public function changeCandidate($iid, $cid)
    {
        $interview = Interview::findOrFail($iid);
        $interview->candidate_id = $cid;
        $interview->save(); //נשמור את הקאנדידיט
        return back();
    }
    public function changeUser($iid, $uid)
    {
        $interview = Interview::findOrFail($iid);
        $interview->user_id = $uid;
        $interview->save(); //נשמור את הקאנדידיט
        return back();
    }
}
