<?php //קובץ קונטרולר שיצרנו

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth; //תרגיל בית 11
use Illuminate\Http\Request;
use App\Candidate; //כדי שיזהה את המודל שיצרנו
use App\User; // כדי שיזהה את המודל שלארבל יצרה ליוזר
use App\Status; //שיזהה את המודל ססטוס
use Illuminate\Support\Facades\Gate; // הרצאה 12
use Illuminate\Support\Facades\Session; //הרצאה 12

// full name is: "App\Http\Controllers\CandidatesController";
class CandidatesController extends Controller //הכל נמצא באותו תיקייה אז זה בסדר
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

     //הקונטרולר הוא רק מנהל הוא שולח את הנתונים לקובצי ויו (אינדקס, אדיט, קריאאט וכו
     
     public function index() // תפקידה להציג לנו קבוצה של אובייקטים שנגדיר
    {
        $candidates = Candidate::all(); //למשוך את כל הרושמות מטבלת קאנדידאטס - רושמים את שם הטבלה ואז אז את שם המודל
        $users = User::all();//מושכים את כל הקאנדידייטס מהדאטה בייס כדי שהויו של אינדקס נקודה בלייד יזהה את כל היוזרים, רושמים את שם הטבלה ואז את שם המודל
        $statuses = Status::all(); //ככה מושכים את כל הסטטוסים מהדאטה בייס, רושמים את שם הטבלה ואז את שם המודל
        return view('candidates.index', compact('candidates','statuses','users')); //כדי שנוכל לראות את הפלט בדפדפן נשלח לויו, שם התיקייה זה קאנדידייטס, והאינקס זה הקובץ ויו, שבתוך התקייה קאנדידייטס, הקומפקט עוזר לזהות את המשתנה גם ללא סימן הדולר בויו אינדקס נקודה בלייד, רושמים את שם המשתנה אבל ללא הדולר
    }

    
    //הרצאה 12
    public function myCandidates()  
    {
        $userId = Auth::id(); //לקבל את האיידי של היוזר המאומת שהתחבר כרגע
        $user = User::findOrFail($userId);
        $candidates = $user->candidates; //לארבל הולכת לריליישנשיפ שהגרדנו, וליור הגדרנו ריליישנשיפ של קאינדיידטייס, ולארבל מריצה שאילתה שמוצאת את כל הקאנדידייטס של היוזר הספציפי
        $users = User::all();
        $statuses = Status::all(); //ככה מושכים את כל הסטטוסים מהדאטה בייס, רושמים את שם הטבלה ואז את שם המודל
        return view('candidates.index', compact('candidates','statuses','users')); //כדי שנוכל לראות את הפלט בדפדפן נשלח לויו, שם התיקייה זה קאנדידייטס, והאינקס זה הקובץ ויו, שבתוך התקייה קאנדידייטס, הקומפקט עוזר לזהות את המשתנה גם ללא סימן הדולר בויו אינדקס נקודה בלייד, רושמים את שם המשתנה אבל ללא הדולר
    }
    
    
    /*
    //תרגיל בית 11    
    public function showMyCandidates(){
        $candidates = Candidate::all();
        $statuses = Status::all();
        $userConnect = Auth::user(); //לקבל את היוזר הרשום המאומת שהתחבר כרגע
        $uidConnect = Auth::id(); //לקבל את האיידי של היוזר המאומת שהתחבר כרגע
        return view('candidates.MyCandidates', compact('candidates','statuses','userConnect','uidConnect'));
    }
*/
    
    
    public function changeStatus($cid, $sid){ //מקבלים מהאתר שלנו את  הקאנדיידיטס איידי ואת הסטטוס איידי
        $candidate = Candidate::findOrFail($cid); //נשלוף את הקאנדיידט מהדאטה בייס, ונוודא אם המועמד נמצא בכלל
        if(Gate::allows('change-status', $candidate))//אם הגייט מרשה לבצע את השינוי סטטוס לקאנדיידיט אז תבצע את כל הפעולות
        {
            $from = $candidate->status->id; //נגדיר משתנה פרום לעמודת הסטטוס איידי מטבלת קאנדידייטס
            if(!Status::allowed($from,$sid)) return redirect('candidates');    //נכניס את המשתנים פרום ואסאיידי לפונקצייה אלוואד מהמודל סטטוס ואם היא מחזירה לי פאלס אז תעשה ישר רידיראקט מבלי לשמור את המועמד    
            $candidate->status_id = $sid; //אם הפונקצייה מחזירה טרו ,נלך לשדה סטטוס איידי בטבלת קאנדידייטס ונהפוך אותו לאסאיידי כך בעצם תישמר בטבלה קאנדידייטס במסד נתונים העמודת סטטוטס איידי הנכונה
            $candidate->save(); //תשמור את הקאנדידייטס
        } else{ //אם אסור לו לשנות את הסטטוס אז נרצה להחזיר אותו חזרה לרישמת הקאנדיידיטס עם הססטוס הקודם שלא התעדכן ונרצה להסביר לו שאין לו את ההרשאה לשנות את הסטטוס, נעשה את זה באמצעות הודעת פלאש
            Session::flash('notallowed','You are not allowed to change the status of the user because you are not the owner of the user!');//רושמים את שם ההודעה, ומלל ההודעה עצמה
        }
        return back();  //נחזיר אחורה ולאו דוווקא לעמוד של קאנדידייטס כי אנחנו רוצים להישאר בעמוד מייקאנדידייטס
    }
    /*
    $candidate = Candidate::findOrFail($cid); // נשלוף את הקאנדיידט מהדאטה בייס, ונוודא אם המועמד נמצא בכלל
    $from = $candidate->status_id; //נגדיר משתנה סטייג' לעמודת הסטטוס איידי מטבלת קאנדידייטס
    $chk = Status::allowed($sid,$from); //נפנה לפונקציה הסטטית אלוואד מהמודל סטטוס שמקבלת כארגומנטים את הססטוס אידיי שהמתשמש רושם ואת המתשנה סטייג שהגדרנו שזה בעצם העמודה ססטוס אידיי בטבלת קאנדידייטס
    if($chk == TRUE){ // אם הפונקצייה אלוואד מחזירה טרו
        $candidate->status_id = $sid; //נלך לשדה סטטוס איידי בטבלת קאנדידייטס ונהפוך אותו לאסאיידי כך בעצם תישמר בטבלה קאנדידייטס במסד נתונים העמודת סטטוטס איידי הנכונה
        $candidate->save(); //תשמור את הקאנדידייטס
        return redirect('candidates');//נשלח לראוט של קאנדידטס שוב, שכך אחרי מה שהפונקצייה תעשה, היא תשלח בחזרה לראוט של קאנדידייטס, בשביל שלא נראה סתם מסך לבן אלא נחזור לראוט של קאנדידייט
        }   
    else{ //אם הפונקצייה אלוואד מחזירה פאלס
        return redirect('candidates');//אל תשמור את המועמד ואל תשנה לו את הסטטוס איידי במסד הנתונים, פשוט ישר תעשה רידירקט ישר לראוט קאנדידייטס
        } 
        */
    
 
    // בעזרת הפונקצייה יהיה אפשר לשנות יוזרים ולבחור ביניהם
    public function changeUser($cid, $uid = null){ // שתי ארגומנטים אחד  למועמד ואחד ליוזר שהמתשנה של היוזר הוא אופציונלי
        Gate::authorize('assign-user'); //נרשום את שם הגייט, את שם היוזר שמחובר כרגע לא צריך לכתוב כי לארבל מכירה אותו אוטומטית כאשר משתמשים בפונקציית גייט
        $candidate = Candidate::findOrFail($cid); // נשלוף את הקאנדיידט מהדאטה בייס, ונוודא אם המועמד נמצא בכלל
        $candidate->user_id = $uid; // נלך לשדה יוזר איידי בטבלת קאנדידייטס ונהפוך אותו ליואידיי, זהו משתנה אופציונלי, וכך בעצם בטבלת קאנדידייטס בשדה יוזר אידיי ישמר בבסיס נתונים היוזר איידי הנכון
        $candidate->save(); //נשמור את הקאנדידיט
        return back(); //נחזיר אחורה ולאו דוווקא לעמוד של קאנדידייטס כי אנחנו רוצים להישאר בעמוד מייקאנדידייטס
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
     public function create() //אמורה להפנות אותי לטופס בו ניצור מועמד חדש
    {
        return view('candidates.create'); //כדי שנוכל לראות את הפלט בדפדפן לראוט של קאנדידטאס קרייאט
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    
     public function store(Request $request)//  כדי לשמור את הנתונים שנוסיף בטופס של קרייאט וכדי שכל מועמד שאנחנו מוסיפים באמת יווצר בטבלת המועמדים
    {
        $candidate = new Candidate(); //יצירת אובייקט חדש ריק
        //$candidate->name = $request->name;//ניתן או להגדיר שדות ספציפיים
        //$candidate->email = $request->email;
        $can = $candidate->create($request->all());//אנחנו נעדיף ליצור הכל מבלי לעדכן במפורש אילו שדות אנחנו נרצה ליצור זה נעשה בזכות התוספת שהוספנו למודל כך הוא לוקח ומזהה את כל השדות מטבלת קאנדידייטס
        $can->status_id = 1; //נוסיף גם שיזהה את השדה סטטוטס אידיי שהוספנו ונבטיח שהדיפולט יהיה 1 לססטוס אידיי כך ברגע שניצור מועמד חדש אז בטבלת קאנדידייטס בעמודה סטטוס אידיי יהיה לו 1 כברירת מחדל
        $can->save();
        return redirect ('candidates');//נשלח חזרה לפונקציה אינדקס שישלח לראוט של קאנדידטס שוב, שכך אחרי מה שהפונקצייה תעשה, היא תשלח בחזרה לראוט של קאנדידייטס, בשביל שלא נראה סתם מסך לבן אלא נחזור לראוט של קאנדידייטס
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
     public function show($id) // יודעת להציג מועמד אחד בודד עם אידיי מסויים
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
   
     public function edit($id) // האובייקט שנרצה לערוך יינתן על ידי ארגומנט האיידיי 
    {
        $candidate = Candidate::findOrFail($id); // נשלוף את הקאנדיידט מהדאטה בייס, ונוודא אם המועמד נמצא בכלל
        return view('candidates.edit', compact('candidate')); //כדי שנוכל לראות את הפלט בדפדפן נקשר לראוט של קאנדידטאס אדיט  וכדי לקשר לויו נרשום בקומפקט את שם המתשנה ללא סימן הדולר  
        // התקייה נקראת קאנדידייטס, והקובץ - ויו, אדיט, נמצא תחת התיקייה הזו, לכן רושמים, קאנדידייטס נקודה אדיט
    }//בזכות הקומפקט בקובץ אדיט זה יודע לזהות את המשתנה קאנדידייט

   
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
     public function update(Request $request, $id)//הפונקצייה עושה את העדכון בפועל, מקבלת גם ריקסוואסט ובניגוד לפונקציית סטור גם איידי
    {
        // דרך שלי בתרגיל בית 7
        //$candidate = Candidate::findOrFail($id);
        //$candidate->name = $request->name; //נאכלס במפורש את השם והאימייל
        //$candidate->email = $request->email;
        //$candidate->update(); // כדי שנוכל לשמור את הנתונים
        //return redirect('candidates'); //כדי שנוכל לראות את הפלט בדפדפן נקשר לראוט של קאנדידטאס
        
        $candidate = Candidate::findOrFail($id); //נשלוף את הקאנדיידט מהדאטה בייס, ונוודא אם המועמד נמצא בכלל
        $candidate->update($request->all()); // כך נוכל לעדכן את הכל מבלי לציין שדות ספציפיות שאותם נרצה לעדכן - זה נעשה בזכות התוספת שהוספנו למודל
        return redirect('candidates');  //נשלח חזרה לפונקציה אינדקס שישלח לראוט של קאנדידטס שוב, שכך אחרי מה שהפונקצייה תעשה, היא תשלח בחזרה לראוט של קאנדידייטס, בשביל שלא נראה סתם מסך לבן אלא נחזור לראוט של קאנדידייטס
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    
     public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id); //נמצא את האלמנט שנרצה למחוק מתוך הדאטה בייס נוודא שהאובייקט באמת קיים
        $candidate ->delete(); // נפעיל את פונקציית דילייט
        return redirect('candidates'); // נשלח חזרה לפונקציה אינדקס שישלח לראוט של קאנדידטס שוב, שכך אחרי מה שהפונקצייה תעשה, היא תשלח בחזרה לראוט של קאנדידייטס, בשביל שלא נראה סתם מסך לבן אלא נחזור לראוט של קאנדידייטס

    }


}
