<?php //מודל חדש שיצרנו בתרגיל בית 10

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB; //ספרייה שהוספנו על מנת לבנות שאילתה

class Department extends Model
{

    //protected $fillable = ['name']; //נוי הוסיפה ואני לא 

    public function users(){ 
        return $this -> hasMany('App\User'); //לכל מחלקה יש כמה יוזרים
    }

    //protected $primaryKey = 'department_id';


}